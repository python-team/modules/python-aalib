python-aalib (0.4-4) unstable; urgency=medium

  * Build-Depend on python3-setuptools, now that distutils is retired.
  * Update standards version to 4.7.0, no changes needed.
  * Bump copyright years.

 -- Stefano Rivera <stefanor@debian.org>  Thu, 05 Sep 2024 11:58:12 +0200

python-aalib (0.4-3) unstable; urgency=medium

  * Bump debhelper from old 12 to 13.

 -- Debian Janitor <janitor@jelmer.uk>  Thu, 26 May 2022 20:07:38 +0100

python-aalib (0.4-2) unstable; urgency=medium

  * Run the ABI test as an autopkgtest. This is more useful than build-time,
    for an arch-all package.

 -- Stefano Rivera <stefanor@debian.org>  Thu, 25 Feb 2021 15:36:36 -0800

python-aalib (0.4-1) unstable; urgency=medium

  [ Stefano Rivera ]
  * New upstream release. (Closes: #982553)
  * Bump copyright years.
  * Add upstream metadata.
  * Bump Standards-Version to 4.5.1, no changes needed.
  * Run the new ABI-test.
    - B-D on libaa1-dev.
  * Install the changelog with dh_installchangelogs. There are no other docs
    to install.
  * Bump watch file to version 4.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.

 -- Stefano Rivera <stefanor@debian.org>  Thu, 25 Feb 2021 14:01:48 -0800

python-aalib (0.3.2-4) unstable; urgency=medium

  * Team upload.
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).

 -- Ondřej Nový <onovy@debian.org>  Fri, 09 Aug 2019 23:21:33 +0200

python-aalib (0.3.2-3) unstable; urgency=medium

  * Use python{3,}-all in autopkgtests, to smooth transitions.
    Thanks Steve Langasek.  (Closes: #864694)

 -- Stefano Rivera <stefanor@debian.org>  Tue, 20 Feb 2018 10:58:03 -0800

python-aalib (0.3.2-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Stefano Rivera ]
  * Replace python{3,}-imaging suggests with python{3,}-pil. (Closes: #866464)
  * Bump copyright years.
  * Bump debhelper compat to 11.
  * Bump Standards-Version to 4.1.3, no changes needed.
  * Declare Rules-Requires-Root: no.

 -- Stefano Rivera <stefanor@debian.org>  Tue, 20 Feb 2018 10:45:45 -0800

python-aalib (0.3.2-1) unstable; urgency=medium

  * New upstream release.
  * Run test during build.
    - Build-Depend on PIL.
  * Add autopkgtest smoke tests.

 -- Stefano Rivera <stefanor@debian.org>  Sat, 30 Jul 2016 21:36:57 -0400

python-aalib (0.3.1-1) unstable; urgency=medium

  * New upstream release.
    - Drops Python 2.5 support.
  * Bump copyright years.
  * Bump Standards-Version to 3.9.8, no changes needed.

 -- Stefano Rivera <stefanor@debian.org>  Sat, 11 Jun 2016 23:16:01 +0200

python-aalib (0.3-2) unstable; urgency=low

  * Canonicalize homepage.
  * Switch watch file to use pypi.debian.net
  * Verify upstream PGP signature.
  * Update VCS URLs for DPMT git migration.
  * Bump Standards-Version, no changes needed.
  * Port to pybuild.
  * Bump debhelper compat level to 9.
  * Bump copyright years.

 -- Stefano Rivera <stefanor@debian.org>  Sun, 14 Feb 2016 21:19:10 -0500

python-aalib (0.3-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Stefano Rivera ]
  * New upstream version.
    - Includes Python 3 support.
  * Bump Standards-Version to 3.9.4
    - Bump debhelper Build-Dep to 8.1 for build-{arch,indep} support.
  * Bump debhelper compat level to 8.
  * Bump copyright years.
  * Bump machine-readable copyright format to 1.0.
  * Re-licence packaging to Expat.
  * Switch debian/watch URL to https.

 -- Stefano Rivera <stefanor@debian.org>  Sun, 26 May 2013 18:55:51 +0200

python-aalib (0.2-3) unstable; urgency=low

  * Upload to unstable.
  * Update my e-mail address.
  * Update copyright format.
  * Drop Breaks: ${python:Breaks}, no longer used by dh_python2.
  * Bump Standards-Version to 3.9.2, no changes needed.
  * Remove trailing commas from *Depends.

 -- Stefano Rivera <stefanor@debian.org>  Sun, 17 Apr 2011 23:13:36 +0200

python-aalib (0.2-2) experimental; urgency=low

  * Upload to experimental to build for Python 2.7 (Closes: #604045)

 -- Stefano Rivera <stefano@rivera.za.net>  Sat, 20 Nov 2010 13:54:36 +0200

python-aalib (0.2-1) unstable; urgency=low

  [ Stefano Rivera ]
  * New Upstream Release
  * Bump Standards-Version to 3.9.1 (no changes needed).
  * Suggest python-imaging
  * Convert to dh_python2
  * Revert X-Python-Version to >=2.5
  * Wrap *Depends in debian/control.

  [ Jakub Wilk ]
  * Update my e-mail address in debian/copyright.

 -- Stefano Rivera <stefano@rivera.za.net>  Sun, 14 Nov 2010 17:48:02 +0200

python-aalib (0.1.1-1) unstable; urgency=low

  * New upstream release.
  * Change XS-Python-Version to all
  * Install upstream changelog and example script.

 -- Stefano Rivera <stefano@rivera.za.net>  Mon, 25 Jan 2010 01:09:34 +0200

python-aalib (0.1-2) unstable; urgency=low

  * copyright: Package maintainer was incorrectly credited as being upstream
    author.

 -- Stefano Rivera <stefano@rivera.za.net>  Sat, 16 Jan 2010 20:12:23 +0200

python-aalib (0.1-1) unstable; urgency=low

  * Initial release (Closes: #564608)

 -- Stefano Rivera <stefano@rivera.za.net>  Mon, 11 Jan 2010 20:49:12 +0200
